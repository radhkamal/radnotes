import React from 'react'
import ReactMarkdown from 'react-markdown';

const Main = ({ activeNote,  updateNote }) => {

  const editField = (key, value) => {
    updateNote({
      ...activeNote,
      [key]: value,
      lastModified: Date.now(),
    })
  };

  if (!activeNote) {
    return <div className="no-active-note">No note selected</div>
  }

  return (
    <div className="app-main">
      <div className="app-main-note-edit">
        <input 
          type="text" 
          id="title" 
          value={activeNote.title} 
          onChange={(event) => editField("title", event.target.value)}
          autoFocus 
        />
        <textarea 
          name="note" 
          id="body" 
          value={activeNote.body} 
          onChange={(event) => editField("body", event.target.value)}
          placeholder='Write your note here...' 
          cols="30" 
          rows="10" 
        />
      </div>
      <div className="app-main-note-preview">
        <h1 className="preview-title">{activeNote.title}</h1>
        <ReactMarkdown className="markdown-preview">
          {activeNote.body}
        </ReactMarkdown>
      </div>
    </div>
  )
}

export default Main
